package SolarSystem;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader scene = new FXMLLoader(getClass().getClassLoader().getResource("main.fxml"));
		stage.setScene(scene.load());
		stage.setTitle("Planetarium");
		stage.setMaximized(true);
		stage.show();
	}

	public static void main(String args[]){
		launch(args);
	}
}
