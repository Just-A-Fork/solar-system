package SolarSystem;

import SolarSystem.classes.CelestialStats;
import SolarSystem.classes.Star;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

	@FXML private Pane root;
	@FXML private Canvas canvas;
	private GraphicsContext g;
	private double Dilation = 1;
	private final double[] Translation = new double[2]
			, DragStart = new double[2];


	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		canvas.heightProperty().bind(root.heightProperty());
		canvas.widthProperty().bind(root.widthProperty());
		g = canvas.getGraphicsContext2D();

		CelestialStats sunStats = new CelestialStats(new double[]{0, 0}, 0, 0, 0,
				5778, 1.989d*Math.pow(10, 30), 695508, 0,
				8.68d*Math.pow(10d, -4d), 8d, true, true);
		Star sun  = new Star(sunStats, Color.RED);

		final long startTime = System.nanoTime();
		new AnimationTimer(){
			@Override
			public void handle(long nanoTime) {
				g.setFill(Color.BLACK);
				g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
				double life = (nanoTime - startTime)/1000000000d;
				g.strokeText(String.valueOf(life), 0, 10);
				g.strokeText(String.valueOf(Translation[0]) + ", " + String.valueOf(Translation[1]), 0, 20);
				g.strokeText(String.valueOf(Dilation), 0, 30);

				sun.draw(canvas, Translation, Dilation);
				g.setStroke(Color.WHITE);
				g.strokeOval(
						canvas.getWidth()/2-300*Dilation+Translation[0],
						canvas.getHeight()/2-300*Dilation+Translation[1],
						600*Dilation,
						600*Dilation);


				double x = 300*Math.cos((life*Math.PI)/4)*Dilation+canvas.getWidth()/2;
				double y = 300*Math.sin((life*Math.PI)/4)*Dilation+canvas.getHeight()/2;
				g.setFill(Color.FORESTGREEN);
				g.fillOval(
						x-10*Dilation+Translation[0],
						y-10*Dilation+Translation[1],
						20*Dilation,
						20*Dilation);

			}
		}.start();
	}

	@FXML void Scroll(ScrollEvent e){
		if(Math.signum(e.getDeltaY()) == 1){
			Dilation*= 1 + e.getMultiplierY()/1000d;

		}else{
			Dilation/= 1 + e.getMultiplierY()/1000d;
		}
	}

	@FXML void Press(MouseEvent e){
		DragStart[0] = e.getX();
		DragStart[1] = e.getY();
	}

	@FXML void Drag(MouseEvent e){
		Translation[0]+= e.getX() - DragStart[0];
		Translation[1]+= e.getY() - DragStart[1];
		DragStart[0] = e.getX();
		DragStart[1] = e.getY();
	}

	private void paint(){
		g.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		g.strokeText("test", 10, 10);
	}
}
