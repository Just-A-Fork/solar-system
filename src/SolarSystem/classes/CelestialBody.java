package SolarSystem.classes;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class CelestialBody {

	private CelestialStats Stats;
	private boolean Orbiting = false;

	public CelestialBody(CelestialStats stats){
		Stats = stats;

		if(stats instanceof SatelliteStats)
			Orbiting = true;
	}

	public CelestialStats getStats() {
		return Stats;
	}

	public boolean isOrbiting() {
		return Orbiting;
	}

	abstract void draw(Canvas canvas, double[] Translation, double Dilation);
}
