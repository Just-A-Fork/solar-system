package SolarSystem.classes;

import javafx.scene.canvas.Canvas;

public class ResizableCanvas extends Canvas {

	public ResizableCanvas(){
		super();
	}

	@Override
	public boolean isResizable() {
		return true;
	}
}
