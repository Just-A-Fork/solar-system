package SolarSystem.classes;

public class SatelliteStats extends CelestialStats {

	public SatelliteStats(double[] position, double velocity, double acceleration, double jounce, double temperature, double mass, double radius, double rotationPeriod, double surfacePressure, double moonCount, boolean ringSystem, boolean magneticField) {
		super(position, velocity, acceleration, jounce, temperature, mass, radius, rotationPeriod, surfacePressure, moonCount, ringSystem, magneticField);
	}


}
