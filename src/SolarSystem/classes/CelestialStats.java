package SolarSystem.classes;

public class CelestialStats {

	private final double[] Position = new double[2];
	private double Velocity;
	private double Acceleration;
	private double Jounce;
	private double Temperature;
	private double Mass;
	private double Radius;
	private double RotationPeriod;
	private double SurfacePressure;
	private double MajorSatelliteCount;
	private boolean RingSystem;
	private boolean MagneticField;
	private final double GravitationalConstant = 6.67408*Math.pow(10, -11);

	public CelestialStats(double[] position, double velocity, double acceleration, double jounce, double temperature,
	                      double mass, double radius, double rotationPeriod, double surfacePressure, double majorSatelliteCount,
	                      boolean ringSystem, boolean magneticField){
		Position[0] = position[0];
		Position[1] = position[1];
		Velocity = velocity;
		Acceleration = acceleration;
		Jounce = jounce;
		Temperature = temperature;
		Mass = mass;
		Radius = radius;
		RotationPeriod = rotationPeriod;
		SurfacePressure = surfacePressure;
		MajorSatelliteCount = majorSatelliteCount;
		RingSystem = ringSystem;
		MagneticField = magneticField;
	}

	public double[] getPosition() {
		return Position;
	}

	public double getVelocity() {
		return Velocity;
	}

	public double getAcceleration() {
		return Acceleration;
	}

	public double getJounce() {
		return Jounce;
	}

	public double getTempeture() {
		return Temperature;
	}

	public double getMass() {
		return Mass;
	}

	public double getRadius() {
		return Radius;
	}

	public double getDensity(){
		return Mass/((4d/3d)*Math.PI*Math.pow(Radius*1000, 3));
	}

	public double getGravityAcceleration() {
		return GravitationalConstant*Mass/Math.pow(Radius*1000, 2);
	}

	public double getEscapeVelocity(){
		return Math.sqrt(2*GravitationalConstant*Mass/(Radius*1000))/1000;
	}

	public double getRotationPeriod() {
		return RotationPeriod;
	}

	public double getSurfacePressure() {
		return SurfacePressure;
	}

	public double getMajorSatelliteCount() {
		return MajorSatelliteCount;
	}

	public boolean isRingSystem() {
		return RingSystem;
	}

	public boolean isMagneticField() {
		return MagneticField;
	}
}
