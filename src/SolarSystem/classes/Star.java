package SolarSystem.classes;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class Star extends CelestialBody {

	Color Color;

	public Star(CelestialStats stats, Color color){
		super(stats);
		Color = color;
	}

	@Override
	public void draw(Canvas canvas, double[] Translation, double Dilation) {
		GraphicsContext g = canvas.getGraphicsContext2D();

		if(isOrbiting()){

		}else{
			CelestialStats stats = getStats();

			Paint tmp = g.getFill();
			g.setFill(Color);


			g.fillOval(
					stats.getPosition()[0] - Dilation*stats.getRadius()/20000d + Translation[0] + canvas.getWidth()/2,
					stats.getPosition()[1] - Dilation*stats.getRadius()/20000d + Translation[1] + canvas.getHeight()/2,
					(stats.getRadius()/10000d)*Dilation,
					(stats.getRadius()/10000d)*Dilation);
			g.setFill(tmp);
		}
	}
}
